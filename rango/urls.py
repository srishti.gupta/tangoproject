from django.conf.urls import url, include
from django.contrib import admin
from rango import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index ,name='views'),
    url(r'^about/$', views.about ,name='about'),
    url(r'^add_category/$', views.add_category, name='add_category'),  # NEW MAPPING!
    url(r'^category/(?P<category_name_slug>[\w\-]+)/$', views.category, name='category'),
    url(r'^register/$', views.register, name='register'), # ADD NEW PATTERN!
    url(r'^login/$', views.user_login, name='login'),
    url(r'^restricted/', views.restricted, name='restricted'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^search/$', views.search, name='search'),
    url(r'^goto/$', views.track_url, name='goto'),
]